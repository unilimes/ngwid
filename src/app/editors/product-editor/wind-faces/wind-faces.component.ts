import { Component, OnInit } from '@angular/core';
import {TopService} from '../../../top/top.service';
import {EditorsService} from '../../editors.service';

@Component({
  selector: 'app-wind-faces',
  templateUrl: './wind-faces.component.html',
  styleUrls: ['./wind-faces.component.less']
})
export class WindFacesComponent implements OnInit {

  constructor(public topService: TopService, public editorsService: EditorsService) { }

  ngOnInit() {
  }

}
