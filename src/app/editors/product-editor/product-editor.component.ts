import {Component, OnInit} from '@angular/core';
import {TopService} from '../../top/top.service';
import {EditorsService} from '../editors.service';

@Component({
    selector: 'app-product-editor',
    templateUrl: './product-editor.component.html',
    styleUrls: ['./product-editor.component.less']
})
export class ProductEditorComponent implements OnInit {

    currentTab = 'geometryAssociation';

    constructor(public topService: TopService, public editorsService: EditorsService) {
    }

    ngOnInit() {
    }

    updateProductAssociation() {
        this.topService.selectedProductAssociation.modifiedBy = this.topService.userData.email;
        this.editorsService.updateProductAssociation(this.topService.selectedProductAssociation)
            .subscribe(
                (response: any) => {
                    if (response.success === true) {
                        alert('Updated!');
                        this.topService.selectedProductAssociation.change = false;
                    }
                },
                error => console.log(error)
            );
    }
}
