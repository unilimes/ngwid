import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TopService} from '../../../top/top.service';
import * as singleObjectsConfig from '../../../shared/singleObjectsConfig.json';
import {EditorsService} from '../../editors.service';

@Component({
    selector: 'app-geometry-association',
    templateUrl: './geometry-association.component.html',
    styleUrls: ['./geometry-association.component.less']
})
export class GeometryAssociationComponent implements OnInit {
    @Output() updateSelectedProductAssociation = new EventEmitter();
    public color = '#127bdc';
    public singleObjectsConfig = singleObjectsConfig;

    constructor(public topService: TopService, public editorsService: EditorsService) {
    }

    ngOnInit() {
    }

    changeProductAssociation() {
        this.topService.selectedProductAssociation.change = true;
    }
}
