import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DocumentsComponent} from '../documents/documents.component';
import {PhotosComponent} from '../photos/photos.component';
import {ProductEditorComponent} from './product-editor.component';
import {GeometryAssociationComponent} from './geometry-association/geometry-association.component';
import {WindFacesComponent} from './wind-faces/wind-faces.component';
import {TopService} from '../../top/top.service';
import {FormsModule} from '@angular/forms';
import {ColorPickerModule} from 'ngx-color-picker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ColorPickerModule
  ],
  declarations: [
    ProductEditorComponent,
    GeometryAssociationComponent,
    WindFacesComponent,
    DocumentsComponent,
    PhotosComponent
  ],
  exports: [ProductEditorComponent],
  providers: [TopService]
})
export class ProductEditorModule {
}
