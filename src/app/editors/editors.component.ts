import { Component, OnInit } from '@angular/core';
import { TopService } from '../top/top.service';

@Component({
  selector: 'app-editors',
  templateUrl: './editors.component.html',
  styleUrls: ['./editors.component.less']
})
export class EditorsComponent implements OnInit {
  constructor(public topService: TopService) { }
  ngOnInit() {
  }

}
