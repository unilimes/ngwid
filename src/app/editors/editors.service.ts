import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as $ from 'jquery';
import {url} from '../shared/service.config';

@Injectable()
export class EditorsService {
    token = $('meta[name="csrf-token"]').attr('content');
    requestOptions = {
        headers: new HttpHeaders().set('X-CSRF-Token', this.token)
    };

    constructor(public http: HttpClient) {
    }

    updateProductAssociation(association) {
        return this.http.put(`${url}/productAssociations/${association.id}`, association, this.requestOptions);
    }

}
