import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { ProductEditorModule } from './product-editor/product-editor.module';
import { GeometryEditorModule } from './geometry-editor/geometry-editor.module';
import {EditorsComponent} from './editors.component';
import { EditorsService } from './editors.service';

@NgModule({
    imports: [
        CommonModule,
        ProductEditorModule,
        GeometryEditorModule
    ],
    declarations: [EditorsComponent],
    exports: [EditorsComponent],
    providers: [EditorsService]
})
export class EditorsModule {
}
