import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeometryEditorComponent } from './geometry-editor.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [GeometryEditorComponent],
    exports: [GeometryEditorComponent]
})
export class GeometryEditorModule { }
