import {NgModule} from '@angular/core';
import {TopModule} from './top/top.module';
import {EditorsModule} from './editors/editors.module';
import {AppComponent} from './app.component';

@NgModule({
    imports: [TopModule, EditorsModule],
    declarations: [AppComponent],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
