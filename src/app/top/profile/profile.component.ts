import {Component, Inject, OnInit} from '@angular/core';
import {UserData} from '../../shared/userdata.interface';
import {TopService} from '../top.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-userdata',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
    fb: FormBuilder;
    showProfile: boolean;
    userData: UserData;
    userDataFormGroup: FormGroup;

    constructor(@Inject(FormBuilder) fb: FormBuilder, private topService: TopService) {
        this.showProfile = false;
        this.userData = {
            id: null,
            first_name: '',
            last_name: '',
            email: '',
            phone: '',
            company: '',
            title: '',
        };
        this.fb = fb;
    }

  ngOnInit() {
      this.topService.getUserData().subscribe(
          (data: UserData) => {
              this.userData = data;

              this.userDataFormGroup = this.fb.group({
                  first_name: [this.userData.first_name, Validators.minLength(2)],
                  last_name: [this.userData.first_name, Validators.minLength(2)],
                  email: {value: this.userData.email, disabled: true},
                  phone: this.userData.phone,
                  company: this.userData.company,
                  title: this.userData.title
              });
          }
      );
  }

  openProfile() {
      this.showProfile = !this.showProfile;
  }

  updateUserData() {
      this.topService
          .updateUserData(this.userDataFormGroup.value, this.userData.id)
          .subscribe(
              status => status ? alert('Saved!') : alert('Can\'t saved for this time'),
              error => console.log(error)
          );
  }

}
