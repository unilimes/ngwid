import {NgModule} from '@angular/core';
import { TreeModule } from 'angular-tree-component';

import {TopService} from './top.service';
import {TopComponent} from './top.component';
import {ProfileComponent} from './profile/profile.component';
import {AccessTypeComponent} from './access-type/access-type.component';
import {DdlSelectorComponent} from './ddl-selector/ddl-selector.component';
import {DataManagementComponent} from './data-management/data-management.component';
import {HttpClientModule} from '@angular/common/http';
import {SelectedProductTreeNodePipe} from './selected-tree-node.pipe';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {ContextMenuModule} from 'ngx-contextmenu';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        TreeModule,
        ContextMenuModule.forRoot()
    ],
    declarations: [
        TopComponent,
        ProfileComponent,
        AccessTypeComponent,
        DdlSelectorComponent,
        DataManagementComponent,
        SelectedProductTreeNodePipe
    ],
    exports: [TopComponent],
    providers: [TopService],
    bootstrap: [TopComponent]
})
export class TopModule {
}
