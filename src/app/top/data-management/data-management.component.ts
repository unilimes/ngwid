import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {jsPanel} from 'jsPanel4';
import {TopService} from '../top.service';
import {Product} from '../../shared/Product';
import {Link} from '../../shared/Link';
import {ContextMenuComponent} from 'ngx-contextmenu';

@Component({
  selector: 'app-data-management',
  templateUrl: './data-management.component.html',
  styleUrls: ['./data-management.component.less']
})
export class DataManagementComponent implements OnInit {
  newProduct: Product;
  showTransferDataPopUp: boolean;
  DRMPanelIsOpen = false;
  @ViewChild('containerForDRMPanel', {read: ElementRef}) containerForDRMPanel: ElementRef;
  @ViewChild('contentForDRMPanel', {read: ElementRef}) contentForDRMPanel: ElementRef;
  @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
  newFolderName: string;


  constructor(public topService: TopService) {
  }

  ngOnInit() {
    this.newFolderName = 'new folder';
    this.newProduct = new Product();
    jsPanel.create({
      content: this.contentForDRMPanel.nativeElement,
      container: this.containerForDRMPanel.nativeElement,
      resizeit: {
        handles: 'n, e, s, w, ne, se, sw, nw',
        minWidth: 930,
        minHeight: 500,
      },
      position: {
        my: 'center',
        at: 'center',
        of: '#topComponent'
      },
      contentSize: {width: 930, height: 500},
      headerControls: {close: 'disable'},
      headerTitle: 'Data Record Management Panel (DRMPanel)'
    });
  }

  toggleDRMPanel() {
    if (this.DRMPanelIsOpen = !this.DRMPanelIsOpen) {
    }
  }

  createFolder(event) {
    console.log(event);
  }

  openTransferPopup() {
    if (this.showTransferDataPopUp = !this.showTransferDataPopUp) {
    }
  }

  copySelectedData() {
    if (this.topService.currentTabMode === 'product') {
      if (this.topService.selectedProduct) {
        const productForCopy = {...this.topService.selectedProduct};

        productForCopy.userID = this.topService.userData.id;
        productForCopy.createdBy = this.topService.userData.email;

        productForCopy.type += '-copy';
        productForCopy.subType += '-copy';
        productForCopy.shape += '-copy';
        productForCopy.manufacture += '-copy';
        productForCopy.model += '-copy-' + new Date().getMilliseconds().toString() + this.topService.userData.id;

        this.topService.createProduct(productForCopy).subscribe(
          (data: any) => {
            productForCopy.id = data.id;
            this.topService.listOfProducts.push(productForCopy);
          },
          error => console.log(error)
        );
        alert('Copy created successfully');
      } else {
        alert('You need select some product.');
        return;
      }
    }
  }

  createLink() {
    if (this.topService.currentTabMode === 'product') {
      if (this.topService.selectedProduct) {
        const newLink = new Link(this.topService.userData.id, 'product', this.topService.selectedProduct.id);
        this.topService.createLink(newLink).subscribe(
          (data: any) => {
            newLink.id = data.id;
            this.topService.listOfLinks.push(newLink);
            console.log(this.topService.listOfLinks);
          }
        );
        alert('Link created successfully');
      } else {
        alert('You need select some product.');
      }
    }
  }

  deleteLink(id) {
    this.topService.deleteLink(id).subscribe(
      () => {
        this.topService.listOfLinks.forEach(link => {
          if (link.id === id) {
            this.topService.listOfLinks.splice(this.topService.listOfLinks.indexOf(link), 1);
          }
        });
        alert('Deleted!');
      },
      error => console.log(error)
    );
  }

  createProduct(newProduct) {
    for (const key in newProduct) {
      if (newProduct.hasOwnProperty(key)) {
        if (key !== 'notes' && newProduct[key] === '') {
          alert('All field are required');
          return;
        }
      }
    }
    const product = JSON.parse(JSON.stringify(newProduct));
    product.createdBy = this.topService.userData.email;
    product.userID = this.topService.userData.id;
    product.mode = 'object';
    this.topService.createProduct(product).subscribe(
      (data: any) => {
        product.id = data.id;
        this.topService.listOfProducts.push(product);
        this.topService.updateDDLs();
        for (const key in newProduct) {
          if (newProduct.hasOwnProperty(key)) {
            newProduct[key] = '';
          }
        }
      },
      error => alert(error.error.message)
    );
  }

  updateProduct(product) {
    product.modifiedBy = this.topService.userData.email;
    this.topService.updateProduct(product).subscribe(
      () => {
        alert('Updated!');
        product.change = false;
        this.topService.updateDDLs();
      },
      error => alert(error.error.message)
    );
  }

  deleteProduct(id) {
    this.topService.deleteProduct(id).subscribe(
      () => {
        this.topService.listOfProducts.forEach(product => {
          if (product.id === id) {
            this.topService.listOfProducts.splice(this.topService.listOfProducts.indexOf(product), 1);
          }
        });
        alert('Deleted!');
        this.topService.updateDDLs();
      },
      error => console.log(error)
    );
  }
}
