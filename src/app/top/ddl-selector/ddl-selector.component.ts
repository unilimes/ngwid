import {Component, OnInit} from '@angular/core';
import {TopService} from '../top.service';

@Component({
    selector: 'app-ddl-selector',
    templateUrl: './ddl-selector.component.html',
    styleUrls: ['./ddl-selector.component.less']
})
export class DdlSelectorComponent implements OnInit {

    constructor(public topService: TopService) {}

    ngOnInit() {
    }
}
