import {Component, OnInit} from '@angular/core';
import {TopService} from '../top.service';


@Component({
  selector: 'app-access-type',
  templateUrl: './access-type.component.html',
  styleUrls: ['./access-type.component.less']
})
export class AccessTypeComponent implements OnInit {
    showPublicAccessPanel: Boolean;


  constructor(public topService: TopService) { }

  ngOnInit() {

  }

  onChangeAccessType() {
      this.topService.updateAccessType();
  }

}
