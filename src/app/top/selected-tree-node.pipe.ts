import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'selectedProductTreeNode'
})
export class SelectedProductTreeNodePipe implements PipeTransform {

    transform(list: any, args?: any, resetList?: any): any {
        if (!args) {
            return resetList;
        }
        if (args === -1) {
            return resetList;
        } else {
            const filteredList = [];
            resetList ? resetList.forEach(element => {
                if (element.parentID === args) {
                    filteredList.push(element);
                }
            }) : console.error();
            return filteredList;
        }
    }

}
