import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as $ from 'jquery';
import * as accessConfig from '../shared/publicAccessConfig.json';
import {url} from '../shared/service.config';
import {UserData} from '../shared/userdata.interface';
import {ProductAssociation} from '../shared/ProductAssociation';
import {SelectedProductTreeNodePipe} from './selected-tree-node.pipe';

@Injectable()
export class TopService {
  token = $('meta[name="csrf-token"]').attr('content');
  treeNodePipe = new SelectedProductTreeNodePipe();
  publicUsers: any;
  currentTabMode = 'product';
  publicData: Boolean = false;
  privateData: Boolean = true;
  listOfProducts: any;
  listOfLinks: any;
  userData: any;
  accessIds = [];
  requestOptions = {
    headers: new HttpHeaders().set('X-CSRF-Token', this.token)
  };

  public selectors: any;
  public ddls: any;
  public treeNodes: any;
  public treeViewOptions: any;
  public productRoot: any;
  public geometryRoot: any;
  public pattern = [];

  selectedProduct = null;
  selectedProductAssociation = new ProductAssociation();

  windFaces = [];

  foldersList: any;
  public filterForPanel: number;
  public resetProductList: Object;

  constructor(private http: HttpClient) {

    this.ddls = {
      type: [],
      subType: [],
      shape: [],
      manufacture: [],
      model: []
    };

    this.treeNodes = [];

    this.treeViewOptions = {
      allowDrag: true,
      allowDrop: true,
      actionMapping: {
        mouse: {
          drop: (tree, node, $event, {from: movedProduct, to}) => {
            movedProduct.parentID = to.parent.data.id;
            tree.update();
            this.listOfProducts = this.treeNodePipe.transform(this.listOfProducts, this.filterForPanel, this.resetProductList);
            this.updateProduct(movedProduct).subscribe(data => console.log(data), error => console.log(error));
          }
        }
      }
    };

    this.selectors = {
      type: null,
      subType: null,
      shape: null,
      manufacture: null,
      model: null
    };
    this.foldersList = [];

    this.productRoot = {
      name: 'Product',
      children: [],
      hasChildren: true,
      id: -1
    };

    this.geometryRoot = {
      name: 'Geometry',
      children: [],
      hasChildren: true,
      id: -2
    };

    this.treeNodes.push(this.productRoot, this.geometryRoot);
    this.getUserData().subscribe(
      (data: UserData) => {
        this.userData = data;
        this.accessIds.push(this.userData.id);


        this.getListOfProducts().subscribe(
          products => this.listOfProducts = this.resetProductList = products,
          error => console.log(error)
        );

        this.getListOfLinks().subscribe(
          links => {
            this.listOfLinks = links;
          },
          error => console.log(error)
        );

        this.getDDLs([]).subscribe(
          ddls => this.ddls = ddls,
          error => console.log(error)
        );
        this.getFolders().subscribe(
          folders => {
            this.foldersList = folders;
            this.formTreeList();
          },
          error => console.log(error),
        );
      }
    );

    this.getPublicUsers(accessConfig).subscribe(
      data => {
        this.publicUsers = data;
        this.publicUsers.forEach(user => {
          user.check = true;
          if (this.publicData && this.accessIds.indexOf(user.id) === -1) {
            this.accessIds.push(user.id);
          }
        });
      },
      error => console.log(error)
    );
  }

  updateFilterForPanel($event) {
    this.filterForPanel = $event.node.data.id;
  }

  updatePattern(byDescriptor) {
    if (this.pattern.length === 0) {
      this.pattern.push({'descriptor': byDescriptor, 'value': this.selectors[byDescriptor]});
    } else {
      let found = false;
      this.pattern.forEach(item => {
        if (item.descriptor === byDescriptor) {
          found = true;
          if (this.selectors[byDescriptor]) {
            item.value = this.selectors[byDescriptor];
            this.pattern.splice(this.pattern.indexOf(item) + 1);
          } else {
            this.pattern.splice(this.pattern.indexOf(item));
          }
        }
      });

      if (!found) {
        this.pattern.push({'descriptor': byDescriptor, 'value': this.selectors[byDescriptor]});
      }
    }

    this.selectors = {
      type: null,
      subType: null,
      shape: null,
      manufacture: null,
      model: null
    };

    this.pattern.forEach(item => {
      this.selectors[item.descriptor] = item.value;
    });

    this.getDDLs(this.pattern).subscribe(
      ddls => {
        let productIsSelected = true;
        for (const key in this.selectors) {
          if (this.selectors.hasOwnProperty(key)) {
            if (this.selectors[key] === null) {
              this.ddls[key] = ddls[key];
            }
            if (this.ddls[key].length === 1) {
              this.selectors[key] = this.ddls[key][0];
            }
            if (this.selectors[key] === null) {
              productIsSelected = false;
            }
          }
        }

        if (productIsSelected) {
          this.getProduct(this.pattern).subscribe(
            data => {
              this.selectedProduct = data;
              this.getProductAssociation(this.selectedProduct.id).subscribe(
                (association: ProductAssociation) => {
                  this.selectedProductAssociation = association;
                  this.selectedProductAssociation.change = false;
                }
              );
            },
            error => console.log(error)
          );
        }
      },
      error => console.log(error)
    );
  }

  getUserData() {
    return this.http.get(`${url}/userData`);
  }

  getPublicUsers(usersIds) {
    return this.http.post(`${url}/publicUsers`, usersIds, this.requestOptions);
  }

  updateUserData(updatedUserData, id) {
    return this.http.put(`${url}/userData/${id}`, updatedUserData, this.requestOptions);
  }

  changeTabMode(mode: string) {
    this.currentTabMode = mode;
  }

  updateAccessType() {
    this.publicUsers.forEach(publicUser => {
      if (this.publicData) {
        if (publicUser.check) {
          if (this.accessIds.indexOf(publicUser.id) === -1) {
            this.accessIds.push(publicUser.id);
          }
        } else {
          this.accessIds.splice(this.accessIds.indexOf(publicUser.id), 1);
        }
      } else {
        this.accessIds.splice(this.accessIds.indexOf(publicUser.id), 1);
      }
    });

    if (this.privateData) {
      if (this.accessIds.indexOf(this.userData.id) === -1) {
        this.accessIds.push(this.userData.id);
      }
    } else {
      this.accessIds.splice(this.accessIds.lastIndexOf(this.userData.id), 1);
    }
    this.updateDDLs();
  }

  getListOfProducts() {
    return this.http.post(`${url}/products/`, {userID: this.userData.id}, this.requestOptions);
  }

  getListOfLinks() {
    return this.http.post(`${url}/links/`, {userID: this.userData.id}, this.requestOptions);
  }

  getDDLs(pattern) {
    return this.http.post(
      `${url}/products/ddls`, {pattern, userIds: this.accessIds}, this.requestOptions);
  }

  updateDDLs() {
    if (this.pattern.length === 0) {
      this.getDDLs([]).subscribe(
        ddls => this.ddls = ddls
      );
    } else {
      this.pattern.length = 0;
      this.selectedProduct = null;
      this.selectors = {
        type: null,
        subType: null,
        shape: null,
        manufacture: null,
        model: null
      };
      this.getDDLs([]).subscribe(
        ddls => this.ddls = ddls
      );
    }
  }

  getProduct(pattern) {
    return this.http.post(`${url}/products/selected`, {pattern, userIds: this.accessIds}, this.requestOptions);
  }

  createProduct(newProduct) {
    return this.http.post(`${url}/products/create`, newProduct, this.requestOptions);
  }

  updateProduct(product) {
    return this.http.put(`${url}/products/${product.id}`, product, this.requestOptions);
  }

  deleteProduct(id) {
    return this.http.delete(`${url}/products/${id}`, this.requestOptions);
  }

  createLink(link) {
    return this.http.post(`${url}/links/create`, link, this.requestOptions);
  }

  deleteLink(id) {
    return this.http.delete(`${url}/links/${id}`, this.requestOptions);
  }

  getProductAssociation(productID) {
    return this.http.post(`${url}/productAssociations`, {productID}, this.requestOptions);
  }

  getFolders() {
    return this.http.post(`${url}/folders/`, {userID: this.userData.id}, this.requestOptions);
  }

  formTreeList() {
    this.foldersList.forEach(item => {
      if (item.parentID > 0) {
        this.foldersList.forEach(folder => {
          if (!folder.children) {
            folder.children = [];
          }
          if (folder.id === item.parentID) {
            folder.children.push(item);
          }
        });
      } else {
        if (item.category === 'product') {
          this.productRoot.children.push(item);
        } else if (item.category === 'geometry') {
          this.geometryRoot.children.push(item);
        }
      }
    });
  }
}
