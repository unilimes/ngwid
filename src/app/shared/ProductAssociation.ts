export class ProductAssociation {
    'id': number;
    'productID': number;
    'geometryType': string;
    'geometryShape': string;
    'd1': number;
    'd2': number;
    'd3': number;
    'd4': number;
    'd5': number;
    'textureType': string;
    'needCalculateAzimuth': boolean;
    'color': string;
    'textureUrl': string;
    'weightWithoutMountingKit': number;
    'weightWithMountingKit': number;
    'weight1': number;
    'weight2': number;
    'weight3': number;
    'weight4': number;
    'weight5': number;
    'ca_f1': number;
    'ca_f2': number;
    'ca_f3': number;
    'ca_f4': number;
    'ca_f5': number;
    'ca_s1': number;
    'ca_s2': number;
    'ca_s3': number;
    'ca_s4': number;
    'ca_s5': number;
    'mt_pipe_size': number;
    'mt_pipe_lth': number;
    'createdBy': string;
    'modifiedBy': string;
    change: boolean;

}
