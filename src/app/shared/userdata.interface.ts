export interface UserData {
    id: number;
    first_name: string;
    last_name: string;
    phone: string;
    company: string;
    title: string;
    email: string;
}
