export class Link {
    id: number;
    userID: number;
    referenceTo: string;
    idReference: number;

    constructor(userID: number, referenceTo: string, idReference: number){
        this.userID = userID;
        this.referenceTo = referenceTo;
        this.idReference = idReference;
    }
}
