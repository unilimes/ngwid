export class Product {
    type: string;
    subType: string;
    shape: string;
    manufacture: string;
    model: string;
    notes: string;
    createdBy: string;
    modifiedBy: string;
    userID: string;
    mode: string;
    linkID: string;

    constructor() {}
}
